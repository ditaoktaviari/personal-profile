@extends('layouts.main')

@section('title', 'Contact')

@section('container')
<!-- Page Title Starts -->
<section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
    <h1>get in <span>touch</span></h1>
    <span class="title-bg">contact</span>
</section>
<!-- Page Title Ends -->
<!-- Main Content Starts -->
<section class="main-content revealator-slideup revealator-once revealator-delay1">
    <div class="container">
        <div class="row">
            <!-- Left Side Starts -->
            <div class="col-12 col-lg-4">
                <h3 class="text-uppercase custom-title mb-0 ft-wt-600 pb-3">Halo !</h3>
                <p class="open-sans-font mb-3">Hubungi saya jika anda memiliki waktu luang untuk berdiskusi.</p>
                <p class="open-sans-font custom-span-contact position-relative">
                    <i class="fa fa-envelope-open position-absolute"></i>
                    <span class="d-block">email</span>dita.oktaviari@undiksha.ac.id
                </p>
                <p class="open-sans-font custom-span-contact position-relative">
                    <i class="fa fa-phone-square position-absolute"></i>
                    <span class="d-block">telp</span>+62 8317 4780
                </p>
                <ul class="social list-unstyled pt-1 mb-5">
                    <li class="facebook"><a title="Facebook" href="https://www.instagram.com/ditaoktaviariii/"><i class="fa fa-facebook" style="margin-top: 25%"></i></a>
                    </li>
                    <li class="twitter"><a title="Instagram" href="https://www.instagram.com/ditaoktaviariii/"><i class="fa fa-instagram" style="margin-top: 25%"></i></a>
                    </li>
                    <li class="youtube"><a title="Youtube" href="https://www.youtube.com/channel/UCBn5JEpSO8IBEZFeAO2CAIg"><i class="fa fa-youtube" style="margin-top: 25%"></i></a>
                    </li>
                    <li class="dribbble"><a title="Dribbble" href="https://www.instagram.com/ditaoktaviariii/"><i class="fa fa-dribbble" style="margin-top: 25%"></i></a>

                    </li>
                </ul>
            </div>
            <!-- Left Side Ends -->
            <!-- Contact Form Starts -->
            <div class="col-12 col-lg-8">
                <form class="contactform" method="post" action="http://slimhamdi.net/tunis/dark/php/process-form.php">
                    <div class="contactform">
                        <div class="row">
                            <div class="col-12 col-md-4">
                                <input type="text" name="name" placeholder="YOUR NAME">
                            </div>
                            <div class="col-12 col-md-4">
                                <input type="email" name="email" placeholder="YOUR EMAIL">
                            </div>
                            <div class="col-12 col-md-4">
                                <input type="text" name="subject" placeholder="YOUR SUBJECT">
                            </div>
                            <div class="col-12">
                                <textarea name="message" placeholder="YOUR MESSAGE"></textarea>
                                <button type="submit" class="btn btn-contact">Send Message</button>
                            </div>
                            <div class="col-12 form-message">
                                <span class="output_message text-center font-weight-600 text-uppercase"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Contact Form Ends -->
        </div>
        {{-- <hr class="separator mt-1">
        <div class="col-12 col-lg-12">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4696.875016545936!2d115.11767810203538!3d-8.143623122332125!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd19043c3b573ab%3A0x5030bfbca8309e0!2sNagasepaha%2C%20Buleleng%2C%20Buleleng%20Regency%2C%20Bali!5e0!3m2!1sen!2sid!4v1632802996565!5m2!1sen!2sid" width="1100" height="400" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </div> --}}
    </div>

</section>

@endsection

