﻿@extends('layouts.main')

<style type="text/css">
.bg2 {
  background-image: url(../img/foto2.jpg);
  background-size: cover;
  background-repeat: no-repeat;
  background-position: top;
  height: calc(100vh - 60px);
  z-index: 111;
  border-radius: 15px;
  left: 30px;
  top: 30px;
  box-shadow: 0 0 7px rgba(0,0,0,.9);
}
</style>

@section('title', 'Home')

@section('container')
<!-- Main Content Starts -->
<section class="container-fluid main-container container-home p-0 revealator-slideup revealator-once revealator-delay1">
    <div class="color-block d-none d-lg-block"></div>
    <div class="row home-details-container align-items-center">
        {{-- <div class="col-lg-4 bg position-fixed d-none d-lg-block"></div> --}}
        <div class="col-lg-4 bg2 position-fixed d-none d-lg-block"></div>
        <div class="col-12 col-lg-8 offset-lg-4 home-details text-left text-sm-center text-lg-left">
            <div>
                {{-- <img src="img/foto.jpg" class="img-fluid main-img-mobile d-none d-sm-block d-lg-none" alt="my picture" /> --}}
                <h6 class="text-uppercase open-sans-font mb-0 d-block d-sm-none d-lg-block">Halo!</h6>
                <h1 class="text-uppercase poppins-font"><span>saya</span> dita oktaviari</h1>
                <p class="open-sans-font">Seorang mahasiswi jurusan Teknik Informatika di Fakultas Teknik dan Kejuruan Undiksha. </p>
                <a href="{{ route('about') }}" class="btn btn-about">tentang saya</a>
            </div>
        </div>
    </div>
</section>
<!-- Main Content Ends -->
@endsection


