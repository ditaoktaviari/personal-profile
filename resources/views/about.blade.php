
@extends('layouts.main')

@section('title', 'About')

@section('container')
{{-- Page Title Starts  --}}
<section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
    <h1>ABOUT <span>ME</span></h1>
    <span class="title-bg">Resume</span>
</section>
{{-- Page Title Ends --}}
{{-- Main Content Starts  --}}
<section class="main-content revealator-slideup revealator-once revealator-delay1">
    <div class="container">
        <div class="row">
            <!-- Personal Info Starts -->
            <div class="col-12 col-lg-5 col-xl-12">
                <div class="row">
                    <div class="col-12">
                        <h3 class="text-uppercase custom-title mb-0 ft-wt-600">info pribadi</h3>
                    </div> 
                    <div class="col-12 d-block d-sm-none">
                        <img src="{{ asset('img/foto.jpg') }}" class="img-fluid main-img-mobile" alt="my picture" />
                    </div>
                    <div class="col-4">
                        <ul class="about-list list-unstyled open-sans-font">
                            <li> <span class="title">Nama Lengkap :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Ni Luh Dita Oktaviari</span> </li>
                            <li> <span class="title">Nama Panggilan :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Dita</span> </li>
                            <li> <span class="title">Tanggal Lahir :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">20 Oktober 2001</span> </li>
                        </ul>
                    </div>
                    <div class="col-4">
                        <ul class="about-list list-unstyled open-sans-font">
                            <li> <span class="title">Alamat :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Rendang, Karangasem, Bali</span> </li>
                            <li> <span class="title">Telepon :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">+6283142714780</span> </li>
                            <li> <span class="title">Email :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">dita.oktaviari@undikhsa.ac.id</span> </li>
                        </ul>
                    </div>
                    <div class="col-4">
                        <ul class="about-list list-unstyled open-sans-font">
                            <li> <span class="title">Kewarganegaraan :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Indonesia</span> </li>
                            <li> <span class="title">Bahasa :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Indonesia, English</span> </li>
                            <li> <span class="title">Bahasa :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Indonesia, English</span> </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Personal Info Ends -->
            <!-- Boxes Starts -->
            {{-- <div class="col-12 col-lg-7 col-xl-6 mt-5 mt-lg-0">
                <div class="row">
                    <div class="col-6">
                        <div class="box-stats with-margin">
                            <h3 class="poppins-font position-relative">12</h3>
                            <p class="open-sans-font m-0 position-relative text-uppercase">years of <span class="d-block">experience</span></p>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="box-stats with-margin">
                            <h3 class="poppins-font position-relative">97</h3>
                            <p class="open-sans-font m-0 position-relative text-uppercase">completed <span class="d-block">projects</span></p>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="box-stats">
                            <h3 class="poppins-font position-relative">81</h3>
                            <p class="open-sans-font m-0 position-relative text-uppercase">Happy<span class="d-block">customers</span></p>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="box-stats">
                            <h3 class="poppins-font position-relative">53</h3>
                            <p class="open-sans-font m-0 position-relative text-uppercase">awards <span class="d-block">won</span></p>
                        </div>
                    </div>
                </div>
            </div> --}}
            <!-- Boxes Ends -->
        </div>
        <hr class="separator">
        <!-- Skills Starts -->
        <div class="row">
            <div class="col-12">
                <h3 class="text-uppercase pb-4 pb-sm-5 mb-3 mb-sm-0 text-left text-sm-center custom-title ft-wt-600">My Skills</h3>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p80">
                    <span>80%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">html</h6>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p80">
                    <span>80%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">css</h6>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p50">
                    <span>50%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">php</h6>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p30">
                    <span>30%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">javascript</h6>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p45">
                    <span>45%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">java</h6>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p70">
                    <span>70%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">MySQL</h6>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p40">
                    <span>40%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">premiere</h6>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p65">
                    <span>65%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">photoshop</h6>
            </div>
        </div>
        <!-- Skills Ends -->
        <hr class="separator mt-1">
        <!-- Experience & Education Starts -->
        <div class="row">
            <div class="col-12">
                <h3 class="text-uppercase pb-5 mb-0 text-left text-sm-center custom-title ft-wt-600">Experience <span>&</span> Education</h3>
            </div>
            <div class="col-lg-6 m-15px-tb">
                <div class="resume-box">
                    <ul>
                        <li>
                            <div class="icon">
                                <i class="fa fa-briefcase" style="margin-top: 30%"></i> 
                            </div>
                            <span class="time open-sans-font text-uppercase">2017</span>
                            <h5 class="poppins-font text-uppercase">Magang <span class="place open-sans-font">Pilar Kreatif Studio</span></h5>
                            <p class="open-sans-font">Pilar Kreatif Studio merupakan peruhaan yang bergerak dalam bidang industri kreatif, yang menerima job marketing, desain website, web hosting, grafik desain, dll.</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 m-15px-tb">
                <div class="resume-box">
                    <ul>
                        <li>
                            <div class="icon">
                                <i class="fa fa-graduation-cap" style="margin-top: 30%"></i> 
                            </div>
                            <span class="time open-sans-font text-uppercase">2019-sekarang</span>
                            <h5 class="poppins-font text-uppercase">S1 <span class="place open-sans-font">Univertitas Pendidikan Ganesha</span></h5>
                            <p class="open-sans-font">Program Studi Pendidikan Teknik Informatika</p>
                        </li>
                        <li>
                            <div class="icon">
                                <i class="fa fa-graduation-cap" style="margin-top: 30%"></i> 
                            </div>
                            <span class="time open-sans-font text-uppercase">2016-2019</span>
                            <h5 class="poppins-font text-uppercase">SMK <span class="place open-sans-font">TI Bali Global Denpasar</span></h5>
                            <p class="open-sans-font">Jurusan Rekayasa Perangkat Lunak</p>
                        </li>
                        {{-- <li>
                            <div class="icon">
                                <i class="fa fa-graduation-cap"></i>
                            </div>
                            <span class="time open-sans-font text-uppercase">2009</span>
                            <h5 class="poppins-font text-uppercase">Bachelor Degree <span class="place open-sans-font">Tunis High School</span></h5>
                            <p class="open-sans-font">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor ut labore</p>
                        </li> --}}
                    </ul>
                </div>
            </div>
        </div>
        <!-- Experience & Education Ends -->
    </div>
</section>
{{-- Main Content Ends --}}
@endsection
